
plot_band_dos_main(band_file_1, dos_file_1, "#placeholder_band", "#placeholder_dos", "#overview", upperLim, lowerLim);
plot_band_dos_main(band_file_2, dos_file_2, "#placeholder_band2", "#placeholder_dos2", "#overview2", upperLim, lowerLim);


//Parameters for plotting
/*


var top_band_bottum = 100.0;
var bottum_band_top = -1000.0;

var image_band;
var image_band_1, image_band_2;//for comparison
var image_dos_1, image_dos_2;
var image_combined;

var circleoff;
var circleon;
if(document.images){
	circleoff=new Image(50,50);
	circleoff.src="img/Select_Zoom.png";
	circleon=new Image(50,50);
	circleon.src="img/Select_Zoom.png";
}
function On(name){
	if(document.images){
		document.images['img'+name].src=circleon.src;
	}
}
function Off(name){
	if(document.images){
		document.images['img'+name].src=circleoff.src;
	}
}
*/
function save_band()
{
	document.location.href = image_band;
	/*
	//ctx = canvas.getContext('2d');

	$save = document.getElementById('save');
	$selections = document.getElementById('selections');
	
	//$imgs = document.getElementById('imgs');
	$img_W= document.getElementById('img_Width');
	$img_H = document.getElementById('img_Height');
	var type = $selections.value;
	var w = $img_W.value;
	var h = $img_H.value;
	//alert("img sizes: " + w + "X" + h);
	Canvas2Image.saveAsImage(canvas, w, h, type, 'customName');
	//alert("end function save_img_bind");
	*/
}
function save_dos()
{
	document.location.href = image_dos;
}
function save_all()
{
	//Get a combined canvas for both band and dos
	//var c = document.createElement("canvas"); 
	var c=document.getElementById("myCanvas");
	var ctx =  c.getContext("2d"); 
	var imageObj1 = new Image();
	var imageObj2 = new Image();
	imageObj1.src = image_band; 
	ctx.drawImage(imageObj1, 0, 0);
	imageObj2.src = image_dos;
	ctx.drawImage(imageObj2, 600, 0);
	image_combined = c.toDataURL(); 
	image_combined = image_combined.replace("image/png","image/octet-stream");
	document.location.href = image_combined;
}
function autoscale(ID)
{
	var placeholder_band;
	var placeholder_dos;
	var placeholder_overview;
	
	if(ID == 1)
	{
		placeholder_band = "#placeholder_band";
		placeholder_dos = "#placeholder_dos";
		placeholder_overview = "#overview";
	}
	else if(ID == 2)
	{
		placeholder_band = "#placeholder_band2";
		placeholder_dos = "#placeholder_dos2";
		placeholder_overview = "#overview2";
	}
	else
		alert('Autoscale: div not found!');
	band_file = band_file_obj[placeholder_band];
	dos_file = dos_file_obj[placeholder_band];
	//alert('band file: '+band_file + '  dos_file:'+dos_file);
	//alert(placeholder_band + placeholder_dos);
	upperLim = top_band_bottum[placeholder_band];
	//alert('upperlim:   '+upperLim);
	lowerLim = -10.01;
	//lowerLim = bottum_band_top;
	////alert('bottum_band_top: '+bottum_band_top);
	plot_band_dos_main(band_file, dos_file, placeholder_band, placeholder_dos, placeholder_overview, upperLim, lowerLim);
}
function reset(ID)
{
	var placeholder_band;
	var placeholder_dos;
	var placeholder_overview;
	
	if(ID == 1)
	{
		placeholder_band = "#placeholder_band";
		placeholder_dos = "#placeholder_dos";
		placeholder_overview = "#overview";
	}
	else if(ID == 2)
	{
		placeholder_band = "#placeholder_band2";
		placeholder_dos = "#placeholder_dos2";
		placeholder_overview = "#overview2";
	}
	else
		alert('Autoscale: div not found!');
	band_file = band_file_obj[placeholder_band];
	dos_file = dos_file_obj[placeholder_band];
	//alert('band file: '+band_file + '  dos_file:'+dos_file);
	//alert(placeholder_band + placeholder_dos);
	upperLim = 10.01;
	lowerLim = -10.01;
	plot_band_dos_main(band_file, dos_file, placeholder_band, placeholder_dos, placeholder_overview, upperLim, lowerLim);
}
/*
function plot_2nd_band()
{
	//document.getElementById("#placeholder3").style.display = 'inline';
	main_div_band = "#placeholder3";
	main_div_dos = "#placeholder4";
	band_file = band_file_original_2;
	dos_file = dos_file_original_2;
	upperLim = 10.01;
	lowerLim = - 10.01;
	$.getJSON(band_file, get_band_data);
}
*/
/*
function hide_2nd_band()
{
	document.getElementById("#placeholder3").style.display = 'none';
}
*/
function image_comparison(){

	////alert(image_band_1);
	////alert(image_band_2);
	var url_1 = image_band_1;
	var url_2 = image_band_2;
	var img_1 = document.createElement("img");
	img_1.src=url_1;//"img/1_1.jpg";
	document.getElementById("img_comparison").appendChild(img_1);
	var img_2 = document.createElement("img");
	img_2.src=url_2;//"img/1_2.jpg";
	document.getElementById("img_comparison").appendChild(img_2);

	
	$(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.7});
	$(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0.3, orientation: 'vertical'});
}

