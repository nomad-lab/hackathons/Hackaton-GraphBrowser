	//----------------Get the labels for x axis-----------------------------------------

	function get_label_flag(array)
	{
		////window.alert('x, y, z:' + array);
		if (array.toString() == [0, 0.5, 0.5].toString())
		{
			return "X";
		}
		else if (array.toString() == [0.5, 0.5, 0.5].toString())
		{
			return "L";
		}
		else if (array.toString() == [0.375, 0.375, 0.75].toString())
		{
			return "K";
		}
		else if (array.toString() == [0.25, 0.5, 0.75].toString())
		{
			return "W";
		}
		else if (array.toString() == [0, 0, 0].toString())
		{
			return "\u0393";
		}
		else if (array.toString() == [0.25, 0.625, 0.625].toString())
		{
			return "U";
		}
		else
		{
			return "?";
		}
	}

	N_labels = 0; //Number of labels (including the repeated ones)
	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		for(var j = 0; j < 2; j++)
			N_labels ++;
	}
	var label_coord_abs = new Array(); //label_coor_abs[N_k_band_segments]: absolute starting(/ending) coordinates of k in each segment
	//var labels_tmp = new Array();
	var label_coord_relative = new Array(); //relative coordinates of k points
	var label_flag = new Array(); //the flag "X", "L", "Gamma", etc. for the labels

	//V01 equilly distributed x labels	
	var i_labels = 0;
	var label_flag_last_final = "";
	var label_flag_current_initial = "";

	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		//For segments 0~N_k_band_segments-2, get only the starting coord; (after checking if the last_coord == current_starting_coord)
		//For segment N_k_band_segments-1, get both starting and end coord.
		var labels_coor_0 =  section_k_band_segment[i_segments]['band_segm_start_end'][0];
		var labels_coor_1 =  section_k_band_segment[i_segments]['band_segm_start_end'][1];
		labels_coor_0 = labels_coor_0.sort(sortNumber);
		labels_coor_1 = labels_coor_1.sort(sortNumber);
		var label_flag_0 = get_label_flag(labels_coor_0);
		var label_flag_1 = get_label_flag(labels_coor_1);
		label_flag_current_initial = label_flag_0;
		if(label_flag_last_final == "")
		{
			label_flag_last_final = label_flag_0;
		}
		if(label_flag_last_final == label_flag_current_initial)
		{
			label_flag[i_labels] = label_flag_current_initial;
		}
		else
		{
			label_flag[i_labels] = label_flag_last_final + '|' + label_flag_current_initial;
		}
		label_flag_last_final = label_flag_1;

		i_labels ++;
		if(i_segments == N_k_band_segments - 1)
		{
			label_flag[i_labels] = label_flag_1;
			i_labels ++;
		}
	}
	//alert(label_flag);
	N_labels = i_labels;

	//Get the coordinates for the labels
	//label_coor_abs = np.zeros((N_labels)) #stores the absolute coordinates of the labels

	for(i_labels = 0; i_labels < N_labels; i_labels ++)
	{
		//Here the evenly-distributed relative coordinates is used, because the coordinates of the labels could be too nerrow when using there absolute coordinates
		//x = labels_coor_0[0]
		//y = labels_coor_0[1]
		//z = labels_coor_0[2]
		//label_coor_abs[i_label] = np.sqrt(x*x+y*y+z*z)
		label_coor_relative[i_labels] = step_k_point * N_k_points_per_segment * i_labels / (1 + 1.0 / N_k_points_all);
	}
