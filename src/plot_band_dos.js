
//Global variables

//var bands_1 = [];
//var dos_1 = [];


//---------TOOLS---------------------
//----Dig out the repeated numbers----
function unique(array){
	var r = [];
	for(var i = 0, l = array.length; i < l; i++) 
	{
		for(var j = i + 1; j < l; j++)
			if (array[i] === array[j]) j = ++i; //9999999999999999: the flag number of band energy of gamma point
		r.push(array[i]);
	}
	return r;
}

//---Sort the array------
function sortNumber(a,b)
{
	return a - b;
}	

//---Get the size of an object----
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function view_data(data)
{
	var inventoryJSONTextAgain = JSON.stringify(data, null,5);
	return inventoryJSONTextAgain;
}

function pad(n, width, z) {
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

function get_label_flag(array)
	{
		////window.//////alert('x, y, z:' + array);
		if (array.toString() == [0, 0.5, 0.5].toString())
		{
			return "X";
		}
		else if (array.toString() == [0.5, 0.5, 0.5].toString())
		{
			return "L";
		}
		else if (array.toString() == [0.375, 0.375, 0.75].toString())
		{
			return "K";
		}
		else if (array.toString() == [0.25, 0.5, 0.75].toString())
		{
			return "W";
		}
		else if (array.toString() == [0, 0, 0].toString())
		{
			return "\u0393";
		}
		else if (array.toString() == [0.25, 0.625, 0.625].toString())
		{
			return "U";
		}
		else
		{
			return "?";
		}
	}
//------------------------------------------

//===================Get the labels for x axis======================================



function plot_band_dos_main(band_file, dos_file, placeholder_band, placeholder_dos, placeholder_overview, upperLim, lowerLim)
{
	/*
	 * should be called from div:
	 *    <script language = "javascript">
	 * 		plot_band_dos_main(band_file, dos_file)
	 *    </script>
	 */
	var image_band;
	var image_dos;
	var band_data_obj = {};
	var dos_data_obj = {};
	var HomoLumodata = {};
	var E_top_valence = 0.0;
	var band_gap_indirect = 0.0;

	//Process the band data
	$.getJSON(band_file, function(band_data){
		band_data_obj = get_band_data(band_data, placeholder_band);
		////////alert(view_data(HomoLumodata));
		E_top_valence = band_data_obj["E_top_valence"];

		//Process the dos data
		$.getJSON(dos_file, function(dos_data){
			dos_data_obj = get_dos_data(dos_data, E_top_valence);
			
			//Plot with flot
			plot_band_dos_flot(band_data_obj, dos_data_obj, placeholder_band, placeholder_dos, placeholder_overview, upperLim, lowerLim);
			
		});//getJSON(dos)
	});//getJSON(band)

}


function get_band_data(band_data, placeholder_band)
{
	var band_data_obj = {};
	var N_bands = 0;
	var k_coor_1D = new Array(); //k_coor_1D[N_k_points_all]: Coordinates of k points in 1 dimension
	var N_k_points_all = 0;
	var N_labels = 0; //Number of labels (including the repeated ones)
	var label_coor_relative = new Array(); //relative coordinates of k points
	var label_flag = new Array(); //the flag "X", "L", "Gamma", etc. for the labels
	//========Begin work======================================

	
	//Read data (along objects)
	section_k_band = band_data['sections']['section_run-0']['sections']['section_single_configuration_calculation-0']['section_k_band'];
	section_k_band_segment = section_k_band[0].section_k_band_segment;

	//Get the number of k band segments
	var N_k_band_segments = section_k_band_segment.length;
	////////alert('Number of k band segments: '+ N_k_band_segments); 

	//--------Get the x axis for the band structure figure: the coordinates of k points in 1D-------------
	//output: k_coor_1D[N_k_points_all]
	// ---> var x = origData['bandPlotXvals'];
	N_k_points_all = 0;
	//var N_k_gamma = 0; //TMP: record the number of gamma point in the band structure points

	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments++)
	{
		////window.//////alert(band_energy)
		var band_k_points = section_k_band_segment[i_segments].band_k_points;
		////window.//////alert('band_k_points.length'+band_k_points.length);
		////window.//////alert(band_k_points);
		for(var i_k_points = 0; i_k_points < band_k_points.length; i_k_points++) //band_k_points.length: number of k points in current segment
		{
			////window.//////alert('j='+j+'band_k_points[j]='+band_k_points[j])
			/* Original: Coor(k) = sqrt(x^2+y^2+z^2)
			x = band_k_points[j][0];
			y = band_k_points[j][1];
			z = band_k_points[j][2];
			////window.//////alert('x, y, z: ' + x+y+z)
			k_coor_1D[N_k_points_all] = Math.sqrt(x * x + y * y + z * z);
			*/
			////window.//////alert(k_coor_1D[j])
			N_k_points_all ++;
		}//for i_k_points
	}//for i_segments
	////window.//////alert('Total number of k points in all segments: ' + N_k_points_all)
	////window.//////alert('Coordinate of k points in 1D: ' + k_coor_1D)


	var step_k_point = 1.0 / N_k_points_all; //Distance between 2 k points on the axis
	step_k_point = (1.0 + step_k_point) / N_k_points_all; // To make the x axis [0, 1]

	for(var i_k_points = 0; i_k_points < N_k_points_all; i_k_points ++)
	{
		k_coor_1D[i_k_points] = (step_k_point * i_k_points).toFixed(3);
	}
	////////alert(k_coor_1D);
	
	
	//--------Get the eigenvalues of each band trajectory--------------
	var N_k_points_per_segment = section_k_band_segment[0].band_energies[0].length
	////window.//////alert('N_k_points_per_segment: ' + N_k_points_per_segment)
	N_bands = section_k_band_segment[0].band_energies[0][0].length; // Number of bands;
	////window.//////alert('N_bands: ' + N_bands)
	var band_energies_all = new Array(); 
		//band_energies_all[N_band_energies_all][N_k_band_segments]: the band energy levels, combining the segments
	for(var i_bands = 0; i_bands < N_bands; i_bands ++)
	{
		band_energies_all[i_bands] = new Array();
	}
	N_k_points_all = 0;
	
	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		var band_energies = section_k_band_segment[i_segments].band_energies;
		////window.//////alert('band_energies:'+ band_energies)
		var N_spin_channel = band_energies.length;
		//if(N_spin_channel > 1) 
			//window.//////alert('Number of spin channel: ' + N_spin_channel + ' is larger than 1 in band data!');//Get only the 1st spin channel !!!FIXME

		//for(var i_spin = 0; i_spin < N_spin_channel; i_spin ++) //Get only the 1st spin channel !!!FIXME
		N_k_points_per_segment = band_energies[0].length; //For safety, get the number of k points in each segment again
		////window.//////alert('N_k_points_per_segment: ' + N_k_points_per_segment)

		for(var i_k_points = 0; i_k_points < N_k_points_per_segment; i_k_points ++)
		{
			for (var i_bands = 0; i_bands < N_bands; i_bands ++)
			{
				band_energies_all[i_bands][N_k_points_all] = band_energies[0][i_k_points][i_bands] / (1.60217656535* Math.pow(10,-19));
			}
			
			N_k_points_all ++;
		}
		
		//////////alert('band energies[band50] :' + band_energies_all[50])
	}
	////////alert('band energies[band1] :' + band_energies_all[0])
	////////alert('N_k_points: ' + N_k_points_all + 'band_energies_all' + band_energies_all);


	//----------------Get the labels for x axis-----------------------------------------


	N_labels = 0; //Number of labels (including the repeated ones)
	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		for(var j = 0; j < 2; j++)
			N_labels ++;
	}
	var label_coord_abs = new Array(); //label_coor_abs[N_k_band_segments]: absolute starting(/ending) coordinates of k in each segment
	//var labels_tmp = new Array();
	var label_coord_relative = new Array(); //relative coordinates of k points
	var label_flag = new Array(); //the flag "X", "L", "Gamma", etc. for the labels

	//V01 equilly distributed x labels	
	var i_labels = 0;
	var label_flag_last_final = "";
	var label_flag_current_initial = "";

	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		//For segments 0~N_k_band_segments-2, get only the starting coord; (after checking if the last_coord == current_starting_coord)
		//For segment N_k_band_segments-1, get both starting and end coord.
		var labels_coor_0 =  section_k_band_segment[i_segments]['band_segm_start_end'][0];
		var labels_coor_1 =  section_k_band_segment[i_segments]['band_segm_start_end'][1];
		labels_coor_0 = labels_coor_0.sort(sortNumber);
		labels_coor_1 = labels_coor_1.sort(sortNumber);
		var label_flag_0 = get_label_flag(labels_coor_0);
		var label_flag_1 = get_label_flag(labels_coor_1);
		label_flag_current_initial = label_flag_0;
		if(label_flag_last_final == "")
		{
			label_flag_last_final = label_flag_0;
		}
		if(label_flag_last_final == label_flag_current_initial)
		{
			label_flag[i_labels] = label_flag_current_initial;
		}
		else
		{
			label_flag[i_labels] = label_flag_last_final + '|' + label_flag_current_initial;
		}
		label_flag_last_final = label_flag_1;

		i_labels ++;
		if(i_segments == N_k_band_segments - 1)
		{
			label_flag[i_labels] = label_flag_1;
			i_labels ++;
		}
	}
	////////alert(label_flag);
	N_labels = i_labels;

	//Get the coordinates for the labels
	//label_coor_abs = np.zeros((N_labels)) #stores the absolute coordinates of the labels

	for(i_labels = 0; i_labels < N_labels; i_labels ++)
	{
		//Here the evenly-distributed relative coordinates is used, because the coordinates of the labels could be too nerrow when using there absolute coordinates
		//x = labels_coor_0[0]
		//y = labels_coor_0[1]
		//z = labels_coor_0[2]
		//label_coor_abs[i_label] = np.sqrt(x*x+y*y+z*z)
		label_coord_relative[i_labels] = step_k_point * N_k_points_per_segment * i_labels / (1 + 1.0 / N_k_points_all);
	}
	////////alert(label_coor_relative);
	

	//-------Get HOMO, LUMO------------------------------------

	var HOMO_global = 0.0;
	var LUMO_global = 0.0;
	var coord_k_point_HOMO = new Array(); // coordinate[x,y,z] of the k point that stores HOMO (relative axis)
	var coord_k_point_LUMO = new Array(); // coordinate of the k point that stores LUMO (relative axis)
	HomoLumodata = {};
	band_gap_indirect = 0.0;
	E_top_valence  = 0.0;

	var tmp_HOMO = 0.0;
	var tmp_HOMO_max = -10000000000.0;
	var tmp_LUMO = 0.0;
	var tmp_LUMO_min = 10000000000.0;
	var tmp_k_coord = new Array();
	var tmp_occ_HOMO, tmp_occ_LUMO, tmp_occ;
	var tmp_k_coord_1D;
	N_k_points_all = 0;
	for(var i_segments = 0; i_segments < N_k_band_segments; i_segments ++)
	{
		var band_occupation = new Array();
		band_occupation = section_k_band_segment[i_segments]["band_occupations"];

		for(var i_k_points = 0; i_k_points < N_k_points_per_segment; i_k_points ++)
		{
			var x = band_k_points[i_k_points][0];
			var y = band_k_points[i_k_points][1];
			var z = band_k_points[i_k_points][2];
			for(var i_bands = 0; i_bands < N_bands; i_bands ++)
			{
				var band_occupation_number = band_occupation[0][i_k_points][i_bands];
				if(band_occupation_number >= 0.5)
				{
					tmp_k_coord[0] = x;
					tmp_k_coord[1] = y;
					tmp_k_coord[2] = z;
					tmp_HOMO = band_energies[0][i_k_points][i_bands] / (1.60217656535* Math.pow(10,-19));
					tmp_occ = band_occupation_number;
					////window.//////alert("HOMO (tmp): " + tmp_HOMO);
				}
			}
			if(tmp_HOMO > tmp_HOMO_max)
			{
				tmp_occ_HOMO = tmp_occ;
				tmp_HOMO_max = tmp_HOMO;
				coord_k_point_HOMO = tmp_k_coord;
			}
			for(var i_bands = N_bands - 1; i_bands >= 0; i_bands --)
			{
				var band_occupation_number = band_occupation[0][i_k_points][i_bands];
				if(band_occupation_number < 0.5)
				{
					tmp_k_coord[0] = x;
					tmp_k_coord[1] = y;
					tmp_k_coord[2] = z;
					tmp_LUMO = band_energies[0][i_k_points][i_bands] / (1.60217656535* Math.pow(10,-19));
					tmp_occ = band_occupation_number;
					////window.//////alert("LUMO (tmp): " + tmp_LUMO);
				}
			}
			if(tmp_LUMO < tmp_LUMO_min)
			{
				tmp_occ_LUMO = tmp_occ;
				tmp_LUMO_min = tmp_LUMO;
				coord_k_point_LUMO = tmp_k_coord;
			}
			////window.//////alert("HOMO, LUMO: " + tmp_HOMO +" | " + tmp_LUMO + "; OCC: " + tmp_occ_HOMO + "|" + tmp_occ_LUMO);
			N_k_points_all ++;
		}
	}
	HOMO_global = tmp_HOMO_max;
	LUMO_global = tmp_LUMO_min;
	band_gap_indirect = LUMO_global - HOMO_global;
	E_top_valence = HOMO_global;
	////alert(HOMO_global);
	////window.//////alert("HOMO, N_occ, coord:" + HOMO_global + "," + tmp_occ_HOMO + "," + coord_k_point_HOMO);
	////window.//////alert("LUMO, N_occ, coord:" + LUMO_global + "," + tmp_occ_LUMO + "," + coord_k_point_LUMO);
	////window.//////alert("Fermi level: " + E_top_valence + ";  band gap: " + band_gap_indirect);
	if(band_gap_indirect < 0)
		band_gap_indirect = 0;

	//window.//////alert("HOMO: " + HOMO_global)

	//Rescale HOMO and LUMO, and store
	HOMO_global = (HOMO_global - E_top_valence).toFixed(3);
	LUMO_global = (LUMO_global - E_top_valence).toFixed(3);
	HomoLumodata["HOMO"] = {};
	HomoLumodata["HOMO"]["energy"] = HOMO_global;
	HomoLumodata["HOMO"]["k"] = coord_k_point_HOMO;
	HomoLumodata["LUMO"] = {};
	HomoLumodata["LUMO"]["energy"] = LUMO_global;
	HomoLumodata["LUMO"]["k"] = coord_k_point_LUMO;
	HomoLumodata["gap"] = band_gap_indirect;
	
	band_data_obj["homo_lumo_data"] = HomoLumodata;
	band_data_obj["E_top_valence"] = E_top_valence;

	
	
	//Rescale the energies with respect to HOMO
	for(var i_k_points = 0; i_k_points < N_k_points_all; i_k_points ++)
	{
		for(var i_bands = 0; i_bands < N_bands; i_bands ++)
		{
			band_energies_all[i_bands][i_k_points] =  band_energies_all[i_bands][i_k_points] - E_top_valence;
		}
	}
	////////alert(band_energies_all);

	top_band_bottum[placeholder_band] = 100.0;
	bottum_band_top[placeholder_band] = -1000.0;
	//alert(view_data(top_band_bottum));
	for(var i_k_points = 0; i_k_points < N_k_points_all; i_k_points ++)
	{
		for (var i_bands = 0; i_bands < N_bands; i_bands ++)
		{
			if(i_bands == N_bands - 1)
			{
				if(top_band_bottum[placeholder_band] > band_energies_all[i_bands][i_k_points])
					top_band_bottum[placeholder_band] = band_energies_all[i_bands][i_k_points];
			}
			if(i_bands == 0)
			{
				if(bottum_band_top[placeholder_band] < band_energies_all[i_bands][i_k_points])
					bottum_band_top[placeholder_band] = band_energies_all[i_bands][i_k_points];
			}
			//band_energies_all[i_bands][i_k_points] = (band_energies_all[i_bands][i_k_points] - dos_fermi_energy).toFixed(3);
		}
	}
	
	//-------------------Store the data-----------------------------
/*	var band_data_obj = {};
	band_data_obj["band_energies_all"] = band_energies_all;
	band_data_obj["k_coor_1D"] = k_coor_1D;
	band_data_obj["labels"] = new Array;
	for(i_labels = 0; i_labels < N_labels; i_labels ++)
	{
		band_data_obj["labels"][i_labels] = new Array(2);
		band_data_obj["labels"][i_labels] = [label_coor_relative[i_labels], label_flag[i_labels]];
	}


	band_data_obj["label_flag"] = label_flag;
	band_data_obj["HOMO"] = HOMO_global;
	band_data_obj["LUMO"] = LUMO_global;
	band_data_obj["band_gap_indirect"] = band_gap_indirect;
*/

	//============================Store the objects=================================================

	
	//--- store the band ----

	band_data_obj["bandPlotXvals"] = k_coor_1D;
	//window.//////alert('k_coor: ' + band_data_obj["bandPlotXvals"])
	var my_band_data = {};
	for(var i_bands = 1; i_bands < N_bands + 1; i_bands ++)
	{
		var name_band = i_bands.toString()
		while(name_band.length < 4)
		{
			name_band = "0" + name_band;
		}
		name_band = "Band" + name_band;
		////window.//////alert('name of band: '+ name_band)
		my_band_data[name_band] = band_energies_all[i_bands - 1];
	}
	////window.//////alert('my_band_data["Band0001"]: ' + my_band_data["Band0001"]);
	band_data_obj["enSpin1"] = my_band_data
	//window.//////alert('Number of points in band_data_obj["enSpin1"]["Band0001"]: ' + band_data_obj["enSpin1"]["Band0001"].length);
	
	//---- store the label ----
	var my_label_tmp = new Array();
	for(var i_labels = 0; i_labels < N_labels; i_labels ++)
	{
		my_label_tmp[i_labels] = new Array();
		my_label_tmp[i_labels][0] = label_coord_relative[i_labels];
		my_label_tmp[i_labels][1] = label_flag[i_labels];
	}
	band_data_obj["labels"] = my_label_tmp;	
	//////alert(view_data(band_data_obj["labels"]));
	//return store_band_data(band_data_obj);
	
	band_data_obj["program_name"] = band_data["sections"][ "section_run-0"]["program_name"];
	band_data_obj["program_basis_set_type"] = band_data["sections"][ "section_run-0"]["program_basis_set_type"];
	band_data_obj["atom_labels"] = band_data["sections"][ "section_run-0"]["sections"]["section_system-0"]["atom_labels"];
	band_data_obj["XC_functional_name"] = band_data["sections"]["section_run-0"]["sections"]["section_method-0"]["section_XC_functionals"][0]["XC_functional_name"] + " + "+ band_data["sections"]["section_run-0"]["sections"]["section_method-0"]["section_XC_functionals"][1]["XC_functional_name"];
	
	return band_data_obj;
}

function get_dos_data(dos_data, E_top_valence)
{
	var dos_energies = {};
	dos_energies = dos_data['sections']['section_run-0']['sections']['section_single_configuration_calculation-0']['section_dos'][0]['dos_energies'][0];
	//N_spin_channel = DOSdata_origin['sections']['section_run-0']['sections']['section_single_configuration_calculation-0']['section_dos'][0]['dos_energies'].length; //"dos_energies"[number_of_spin_channels][number_of_dos_values]
	////window.//////alert('Number of spin channels in dos: ' + N_spin_channel);
	//if(N_spin_channel > 1) 
	//		window.//////alert('Number of spin channel: ' + N_spin_channel + ' is larger than 1.');//Get only the 1st spin channel !!!FIXME
	var dos_values =  dos_data['sections']['section_run-0']['sections']['section_single_configuration_calculation-0']['section_dos'][0]['dos_values'][0];
	var N_dos_values = dos_values.length;
	var dos_fermi_energy = dos_data['sections']['section_run-0']['sections']['section_single_configuration_calculation-0']['section_dos'][0]['dos_fermi_energy']
	for(var i_dos_values = 0; i_dos_values < N_dos_values; i_dos_values ++)
	{
		//dos_energies[i_dos_values] = ((dos_energies[i_dos_values] - dos_fermi_energy) / (1.60217656535* Math.pow(10,-19))).toFixed(3); //rescale the energies wrt dos fermi energy
		dos_energies[i_dos_values] = dos_energies[i_dos_values] / (1.60217656535* Math.pow(10,-19));
		dos_energies[i_dos_values] = (dos_energies[i_dos_values] - E_top_valence) .toFixed(3);
	}
	/*
	dos_data_obj = {};
	dos_data_obj["dos_values"] = dos_values;
	dos_data_obj["dos_energies"] = dos_energies;
	*/

	var dos_data_obj = {};
	var DOSdata_enSpin1 = {};
	DOSdata_enSpin1["dos"] = dos_values;
	DOSdata_enSpin1["energies"] = dos_energies;
	dos_data_obj["enSpin1"] = DOSdata_enSpin1;
	
	////////alert('dos: '+view_data(dos_data_obj));
	
	return dos_data_obj;

}

function plot_band_dos_flot(origData, DOSdata, placeholder_band, placeholder_dos, placeholder_overview, upperLim, lowerLim)
{

//--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!PLOT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//alert('lim:'+upperLim+'  '+ lowerLim);
	
	var NenBands = Object.size(origData['enSpin1']);
	////window.//////alert('NenBands: '+ NenBands)
	
	
	/*  TEST
	labels = origData.labels
	Nlabels = Object.size(labels)
	//window.//////alert('labels: '+ labels)
	var inventoryJSONTextAgain = JSON.stringify(origData, null,5);
	//////alert(inventoryJSONTextAgain)
	document.write(inventoryJSONTextAgain)
	//origData['labels'] = [];
	//origData['enSpin1'] = {};
	//origData['bandPlotXvals'] = [];
	*/
	
	
	
	//--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
	//   $.getJSON("material/"+material+"/"+method+"/bandsHomoLumo.json", function(HomoLumodata){
	var gap = HomoLumodata.gap.toFixed(2)
	var kHOMOx = HomoLumodata.HOMO.k[0].toFixed(1)
	var kHOMOy = HomoLumodata.HOMO.k[1].toFixed(1)
	var kHOMOz = HomoLumodata.HOMO.k[2].toFixed(1)   
	var kLUMOx = HomoLumodata.LUMO.k[0].toFixed(1)
	var kLUMOy = HomoLumodata.LUMO.k[1].toFixed(1)
	var kLUMOz = HomoLumodata.LUMO.k[2].toFixed(1) 
	var gapStr = ""
	if (gap > 0.1) {
		gapStr = "The band gap is <nobr>"+String(gap)+" eV</nobr>. The valence band maximum (VBM) is at <b><i>k</i></b>-point <nobr>("+kHOMOx+", "+kHOMOy+", "+kHOMOz+") &#x212b;<sup>-1</sup>.</nobr> The conduction band minimum (CBM) is at <b><i>k</i></b>-point <nobr>("+kLUMOx+", "+kLUMOy+", "+kLUMOz+") &#x212b;<sup>-1</sup>.</nobr><br><br>"
	}
	else if (gap < 0.1) {
		$("#choices").hide()
	}
	
	labels = origData.labels
	Nlabels = Object.size(labels)
	////window.//////alert('Before DOS: labels: '+ labels)
	
	//$.getJSON("material/"+material+"/"+method+"/dosPlotData.json", function(DOSdata){
	bands = []
	dos = []
	zeroline = []
	
	Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
	};
	
	
	labels = origData.labels
	Nlabels = Object.size(labels)
	//window.//////alert('labels: '+ labels)
	
	for (var ilabel = 0; ilabel < Nlabels; ilabel +=1) {
	if ( labels[ilabel][1]== "Gamma" || labels[ilabel][1]== "gamma" ) { labels[ilabel][1] = "\u0393" }
	else if ( labels[ilabel][1]== "Lambda" || labels[ilabel][1]== "lambda" || labels[ilabel][1]== "Lamda" || labels[ilabel][1]== "Lamda" ) { labels[ilabel][1] = "\u039B" }
	else if ( labels[ilabel][1]== "Sigma" || labels[ilabel][1]== "sigma" ) { labels[ilabel][1] = "\u03A3" }
	};
	//console.log(labels[1])
	
	function MyFormatter(v, axis) {
	return " ";
	}
	
	function pad(n, width, z) {
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	};
	
	// Get the number of energy bands
	var NenBands = Object.size(origData['enSpin1']);
	////window.//////alert('NenBands: '+ NenBands)
	
		//var x = origData['bandPlotXvals'];
		//for (var iband = 12; iband < 16; iband += 1) {
	//	var d1 = [];
	//	var y = origData['enSpin1']['Band00'+String(iband)];
	//	for (var i = 0; i < x.length; i+=1) {
	//	  d1.push([x[i],y[i]]);
	//	}
	//  plotData={ label:"band"+String(iband), data: d1}
	//  bands.push(plotData)
	//}
	
	
	bandsd1 = []
	var x = origData['bandPlotXvals'];
	//    var a = origData['enSpin1']['Band0001']
	for (var iband = 1; iband < NenBands+1; iband += 1) {
		var d1 = [];
		var y = origData['enSpin1']['Band'+String(pad(iband,4))];
		for (var i = 0; i < x.length; i+=1) {
		d1.push([x[i],y[i]]);
		}
		plotData={ shadowSize: 0 , data: d1, color: 'black',  series: { lines: { show: true } , points: {show: false} } }
		bands.push(plotData)
	}

	var xdos = DOSdata['enSpin1']['energies'];
	var ydos = DOSdata['enSpin1']['dos'];
	//window.//////alert("xdos: " + xdos);
	//window.//////alert( "ydos: " + ydos);
	var dosLabelPos = Math.max.apply(Math, ydos)/2;
	var d2 = [];
	for (var i = 0; i < xdos.length; i+=1) {
		d2.push([ydos[i],xdos[i]]);
	}
	plotData2={ shadowSize: 0, color: 'black' , data: d2}
	dos.push(plotData2)
	
	
	
	var d3 = [[0, 0], [1, 0]]
	plotDataZero={ shadowSize:0, color: 'black', data: d3, dashes:{ show:true, lineWidth: 1.5}}
	bands.push(plotDataZero)
	
	
	// To optimize the label position, change 'else { labelPos = "below" }' to coordinates, check LiCl LDA
	var d5 = []
	var d5label =[]
	var homo = HomoLumodata.HOMO.energy
	var lumo = HomoLumodata.LUMO.energy
	var x = origData['bandPlotXvals'];
	var iVBM = 0;
	var iCBM = 0;
	var labelPos = "";
	for (var iband = 1; iband < NenBands+1; iband += 1) {
		var y = origData['enSpin1']['Band'+String(pad(iband,4))];
		for (var i = 0; i < x.length; i+=1) {
			if( Math.abs( y[i]-HomoLumodata.HOMO.energy) < 0.008 ){
			d5.push([x[i],y[i]]) ;
			iVBM = iVBM+1;
			if (iVBM === 1){
					d5label.push("VBM") 
		}
		else if (iVBM > 1){
					d5label.push("") 
		}
			}
			else if (y[i] === lumo ) {
				d5.push([x[i],y[i]]) ;
			iCBM = iCBM+1;
		if (iCBM === 1){
					d5label.push("CBM") ;
						if (x[i] < 0.02) { labelPos = "below" }
			else { labelPos = "below" }
		}
		else if (iCBM > 1){
					d5label.push("") 
		}
		}
		}     
	}
	plotHOMOLUMO={ shadowSize:0, color: 'red', data: d5, points:{show:true, radius: 0.8 , fill: true}, 
				showLabels: true, labels: d5label, labelPlacement: labelPos, canvasRender: true, cColor: 'red', cFont:"1em Arial"  }
	//if ($("#showVBM:checked").length > 0 && gap > 0.1) 
	if (gap > 0.1)
	{
		bands.push(plotHOMOLUMO) 
	}


	//----PLOT-----------------------------
	
	var canvas_band;
	var canvas_dos;


	//window.//////alert("Upper limit: " + upperLim + "lower limit: " + lowerLim);
	//window.//////alert("HOMO: " + homo + "LUMO: " + lumo);

	var options = {  //imageClassName: "canvas-image",
		series: {
			lines: { show: true, lineWidth: 2 },
			points: { show: false }
		},
		xaxis: {
			ticks: origData.labels,
			color:"rgb(0, 0, 0)",
			font: {size: 20},
			//zoomRange: [0.1, 10], 
			//panRange: [-10, 10]
		},
		yaxis: {
			axisLabel: "Energy (eV)",
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 20,
			axisLabelFontFamily: 'Arial',
			axisLabelPadding: 10,
			color:"rgb(0, 0, 0)",
			font: {size: 20},
			tickLength:-5,
			min: lowerLim,
			max: upperLim,
			tickDecimals: 0,
			//zoomRange: [0.1, 10], 
			//panRange: [-50, 50]
		},
		//zoom: {
		//	interactive: true
		//},
		//pan: {
		//	interactive: true
		//},
		selection: { mode: "xy", color: "#86a6b4" },
		
		grid: {
			labelMargin: 15,
			hoverable: true,
			backgroundColor: { colors: ["#fff", "#fff"] },
			borderWidth: {
				top: 2,
				right: 2,
				bottom: 2,
				left: 2,
				color : null
			}
		}
	}

	var plot_band = $.plot($(placeholder_band), bands, options);
	canvas_band = plot_band.getCanvas();
	//if(placeholder_band == "#placeholder")
	//{
	//	bands_1 = bands;
	//	dos_1 = dos;
	//}


	var options_DOS ={
		series: {
			lines: { show: true, lineWidth:2 },
			points: { show: false }
		},
		
		yaxis: {
			color:"rgb(0, 0, 0)",
			font: {size: 20},
			ticks:[],
			tickLength:-5,
			min: lowerLim,//-100.00,
			max: upperLim,//-(-100.00),
			// tickFormatter: MyFormatter
			//transform: function (v) { return -v; },
			//inverseTransform: function (v) { return -v;}
			//panRange: [-10, 10]
		},
		
		
		xaxis: {
			axisLabel: " ",
			axisLabelUseCanvas: false,
			axisLabelFontSizePixels: 20,
			axisLabelFontFamily: 'Arial',
			axisLabelPadding: 3,
			color:"rgb(0, 0, 0)",
			font: {size: 20},
			//ticks: 10,
			tickLength:0,
			//min: 0,
			//max: 1,
			tickDecimals: 0,
			ticks: [[ dosLabelPos ,'DOS']],
			//tickFormatter: MyFormatter
			//panRange: [-10, 10]
		},
		
		/*
		pan: {
			interactive: true,
			cursor: "move",    // CSS mouse cursor value used when dragging, e.g. "pointer"
			frameRate: 20
		},
		*/
	
		grid: {
			labelMargin: 15,
			hoverable: true,
			//borderWidth : 1000,
			//show : false,
			//  //backgroundColor: { colors: [ "#fff", "#eee" ] },
			backgroundColor: { colors: ["#fff", "#fff"] },
			borderWidth: {
			top: 2,
			right: 2,
			bottom: 2,
			left: 2,
			color : null
			}
		}
	}
	


	var plot_dos = $.plot(placeholder_dos, dos, options_DOS );
	canvas_dos = plot_dos.getCanvas(); // save the canvas


	//function combine_imgs()
	/*{ 
			//Get a combined canvas for both band and dos
			//var c = document.createElement("canvas"); 
			var c=document.getElementById("myCanvas");
			var ctx =  c.getContext("2d"); 
			var imageObj1 = new Image();
			var imageObj2 = new Image();
			imageObj1.src = image_band; 
			ctx.drawImage(imageObj1, 0, 0);
			imageObj2.src = image_dos;
			ctx.drawImage(imageObj2, 600, 0);
			image_combined = c.toDataURL(); 
			image_combined = image_combined.replace("image/png","image/octet-stream");
	}*/




	
	//------------Select and zoom---------------------
	// setup overview
	
	var overview = $.plot(placeholder_overview, bands, {
		legend: { show: true, container: $("#overviewLegend") },
		series: {
			lines: { show: true, lineWidth: 1 },
			shadowSize: 0
		},
		xaxis: { ticks: origData.labels, axisLabelFontFamily: 'Arial'},
		yaxis: { min: lowerLim, max: upperLim, axisLabelFontFamily: 'Arial' },
		grid: { color: "#000" },
		selection: { mode: "xy", color: "#86a6b4" },
		shadowSize: 10
	});
	// now connect the two
	function find_xaxis_max_min(data, lowerbound, upperbound)
	{
		//data[xaxis,y] find the max and min value of x axis
		////////alert("dos[0]:" + data[0]["data"]);
		////////alert("Bounds: " + lowerbound + " ,  " + upperbound)
		var n_data = data.length;
		////////alert("N dos: " + n_data);
		var data_min = 1000000;
		var data_max = -1000000;
		var data_current = 0;
		for (var i = 0; i < n_data; i++)
		{
			if((data[i][1] >= lowerbound) && (data[i][1] <= upperbound))
			{
				data_current = data[i][0];
				////////alert("data[i]: " + data[i] + "data[i][0]: " + data_current);
				if(data_current >= data_max)
				{
					data_max = data_current;
				}
				if(data_current <= data_min)
				{
					data_min = data_current;
				}
			}
		}
		data_max_min = [data_max, data_min];
		////////alert("data_max_min: " + data_max_min);
		return data_max_min;
	}
	
	$(placeholder_band).bind("plotselected", function (event, ranges) {
		
		if(placeholder_band == "#placeholder_band")
		{
			bands = bands_1;
			dos = dos_1;
		}
		if(placeholder_band == "#placeholder_band2")
		{
			bands = bands_2;
			dos = dos_2;
		}
		
		////////alert(view_data(ranges));
		// clamp the zooming to prevent eternal zoom
		if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
			ranges.xaxis.to = ranges.xaxis.from + 0.00001;
		if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
			ranges.yaxis.to = ranges.yaxis.from + 0.00001;
		// do the zooming
		//alert('placeholder:'+placeholder_band);
		plot_band = $.plot($(placeholder_band), bands,
					$.extend(true, {}, options, {
						xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
						yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
					}));
		canvas_band = plot_band.getCanvas(); // save the canvas
		image_band = canvas_band.toDataURL();
		image_band = image_band.replace("image/png","image/octet-stream");
				
		
		//combine_imgs();
		//image_combined = image_band;
		var data_max_min=new Array();
		////////alert(view_data(dos));
		data_max_min = find_xaxis_max_min(dos[0]["data"], ranges.yaxis.from, ranges.yaxis.to);
		dosLabelPos = (data_max_min[0] - data_max_min[1])/2;
		plot_dos = $.plot(placeholder_dos, dos, 
					$.extend(true, {}, options_DOS, {
						xaxis: { min: data_max_min[1], max: data_max_min[0],tickLength:0,tickDecimals: 0, ticks: [[ dosLabelPos ,'DOS']]},
						yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
					}));
		canvas_dos = plot_dos.getCanvas(); // save the canvas
		image_dos = canvas_dos.toDataURL();
		image_dos = image_dos.replace("image/png","image/octet-stream");


		//combine_imgs();
		// don't fire event on the overview to prevent eternal loop
		overview.setSelection(ranges, true);
	});
	$(placeholder_overview).bind("plotselected", function (event, ranges) {
		plot_band.setSelection(ranges);
	});


	//Get the URL of images for save
	image_band = canvas_band.toDataURL();
	image_band = image_band.replace("image/png","image/octet-stream");
	image_dos = canvas_dos.toDataURL();
	image_dos = image_dos.replace("image/png","image/octet-stream");
	//combine_imgs();

	//imageObj1.src = image_band; 
	//ctx.drawImage(imageObj1, 0, 0);
	//imageObj2.src = image_dos;
	//ctx.drawImage(imageObj2, 600, 0);
	//image_combined = c.toDataURL(); 
	//image_combined = image_combined.replace("image/png","image/octet-stream");			
		
		



	//CBM and VBM //FIX ME: rescale after zooming

	$("<div id='tooltip'></div>").css({
		position: "absolute",
		display: "none",
		border: "0.5px solid #333",
		// padding: "3px",
		"background-color": "#F7F7F7",
		opacity: 0.90
	}).appendTo("body");
	
	$(placeholder_band).bind("plothover", function (event, pos, item) {
		//if ($("#enableTooltip:checked").length > 0) {
			if (item)
			{
				if( Math.abs(item.datapoint[1]-HomoLumodata.HOMO.energy) < 0.005)
				{
					var h = HomoLumodata.HOMO.energy.toFixed(5);
					var x = item.datapoint[0].toFixed(2) ,
					y = Math.abs(item.datapoint[1]).toFixed(2);
					$("#tooltip").html( "VBM, Energy: " + y +" eV")
						.css({top: item.pageY+5, left: item.pageX+5})
										.fadeIn(200);
				}   
				else if ( Math.abs( item.datapoint[1]- HomoLumodata.LUMO.energy) < 0.005)
				{
					y = item.datapoint[1].toFixed(2);
					$("#tooltip").html( "CBM, Energy: " + y +" eV")
					.css({top: item.pageY+5, left: item.pageX+5})
						.fadeIn(200);
				}
				else{
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);
					$("#tooltip").html( " Energy: " + y +" eV")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
				}
			}
			else 
			{
				$("#tooltip").hide();
			}
		//}
	});
			
	$(placeholder_dos).bind("plothover", function (event, pos, item) {
		//if ($("#enableTooltip:checked").length > 0) {
		if (item) {
			var x = item.datapoint[0].toFixed(2),
				y = item.datapoint[1].toFixed(2);
			$("#tooltip").html( " Energy: " + y +" eV")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(200);
		} 
		else 
		{
			$("#tooltip").hide();
		}

	});


	if((placeholder_band == "#placeholder_band") && (upperLim == 10.01) &&(lowerLim == - 10.01))
	{
		;//alert('band 1!');
		image_band_1 = image_band;
		bands_1 = bands;
		dos_1 = dos;
		document.getElementById('atom_labels_1').innerHTML = "<b>Atom labels:</b> &nbsp&nbsp&nbsp&nbsp" + origData["atom_labels"];
		document.getElementById('program_name_1').innerHTML = "<b>Code:</b>&nbsp&nbsp&nbsp&nbsp" + origData["program_name"];
		document.getElementById('program_basis_set_type_1').innerHTML = "<b>Basis set type:</b>&nbsp&nbsp&nbsp&nbsp" + origData["program_basis_set_type"];
		document.getElementById('XC_functional_name_1').innerHTML = "<b>Functional:</b>&nbsp&nbsp&nbsp&nbsp" + origData["XC_functional_name"];
		
	}
	else if((placeholder_band == "#placeholder_band2") && (upperLim == 10.01) &&(lowerLim == - 10.01))
	{
		;//alert('band 2!');
		image_band_2 = image_band;
		bands_2 = bands;
		dos_2 = dos;
		document.getElementById('atom_labels_2').innerHTML = "<b>Atom labels:</b> &nbsp&nbsp&nbsp&nbsp" + origData["atom_labels"];
		document.getElementById('program_name_2').innerHTML = "<b>Code:</b>&nbsp&nbsp&nbsp&nbsp" + origData["program_name"];
		document.getElementById('program_basis_set_type_2').innerHTML = "<b>Basis set type:</b>&nbsp&nbsp&nbsp&nbsp" + origData["program_basis_set_type"];
		document.getElementById('XC_functional_name_2').innerHTML = "<b>Functional:</b>&nbsp&nbsp&nbsp&nbsp" + origData["XC_functional_name"];
	}
	else 
	{
	}
	

	
	//$("#description1_vis" ).html("Band structure and density of states of "+material+" in "+structure+" structure, lattice parameters optimized with "+method+", band structure calculated with "+baseMethod+'.');

}//end function plot_band_dos_flot
