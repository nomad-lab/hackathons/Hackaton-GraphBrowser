params={};
window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,
	function(str,key,value) {
		params[key] = value;
	});

base = "/user/src/tmp/"	
//----------------Input files -------------------------------
band_file_original_1 = base + params['band_file'];
band_file_original_2 = base + params['band_file2'];
dos_file_original_1 = base + params['dos_file'];
dos_file_original_2 = base + params['dos_file2'];
//var dos_file = 'data/P1etRNSyX01fCnpA_StJhXrC7bCpp.json';
//-----------------------------------------------------------


var band_file_1 = band_file_original_1;
var dos_file_1 = dos_file_original_1;
var band_file_2 = band_file_original_2;
var dos_file_2 = dos_file_original_2;

var band_file_obj = {};
var dos_file_obj  = {};

var upperLim = 10.01;
var lowerLim = - 10.01;


var top_band_bottum = {};
var bottum_band_top = {};

var bands_1 = new Array(); // To record the initial band data for each material
var bands_2 = new Array();
var dos_1 = new Array();
var dos_2 = new Array();

band_file_obj['#placeholder_band'] = band_file_original_1;
dos_file_obj['#placeholder_band'] = dos_file_original_1;
band_file_obj['#placeholder_band2'] = band_file_original_2;
dos_file_obj['#placeholder_band2'] = dos_file_original_2;